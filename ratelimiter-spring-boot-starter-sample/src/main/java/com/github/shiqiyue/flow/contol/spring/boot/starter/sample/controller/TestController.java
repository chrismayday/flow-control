package com.github.shiqiyue.flow.contol.spring.boot.starter.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * 测试控制器
 * 
 * @author wwy
 *
 */
@RestController
@RequestMapping("/test")
public class TestController {
	
	@GetMapping("f1")
	public String test1() {
		return "test1";
	}
	
	@GetMapping("f2")
	public String test2() {
		return "test1";
	}
	
	@GetMapping("f1/s1")
	public String test3() {
		return "test1";
	}
	
	@GetMapping("f2/s2")
	public String test4() {
		return "test1";
	}
	
}
